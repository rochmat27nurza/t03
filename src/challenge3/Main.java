package challenge3;

public class Main {
   public static void main(String[] args) {
      Anjing anjing = (Anjing) new Hewan(); //hasilnya tidak keluar meskipun tidak ada error
      anjing.eat();
   }
   // Jika menggunakan kode program ini hasilnya keluar
   // public static void main(String[] args) {
   //    Hewan hewan = new Anjing();
   //    Anjing anjing = (Anjing) hewan;
   //    anjing.eat();
   // }
}